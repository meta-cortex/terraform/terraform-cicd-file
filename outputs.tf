output "name" {
  description = "Name of the Prefix list"
  value       = aws_ec2_managed_prefix_list.prefix_list.name
}

output "id" {
  description = "ID of the Prefix list"
  value       = aws_ec2_managed_prefix_list.prefix_list.id
}
