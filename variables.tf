variable "prefix_list_name" {
  description = "Name of the prefix list"
  type        = string
  default     = "All Web Server Subnet CIDRs"
}

variable "prefix_list_max_entries" {
  description = "Enter the maximum number of entries for the prefix list"
  type        = number
  default     = 3
}
