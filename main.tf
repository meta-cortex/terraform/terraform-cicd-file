terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}


resource "aws_ec2_managed_prefix_list" "prefix_list" {
  name           = var.prefix_list_name
  address_family = "IPv4"
  max_entries    = var.prefix_list_max_entries

  entry {
    cidr        = "10.10.1.0/32"
    description = "Subnet A"
  }

  entry {
    cidr        = "10.10.2.0/32"
    description = "Subnet B"
  }

  entry {
    cidr        = "10.10.3.0/32"
    description = "Subnet C"
  }

  tags = {
    Env         = "Web Servers"
    Terraform   = "true"
    Environment = "dev"
  }
}
